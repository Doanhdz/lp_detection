import numpy as np
from cv2 import cv2 
import sys
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
net=cv2.dnn.readNet('/home/doanhdz/Desktop/Computer vision/Lp_Detection/data/ocr/ocr-net.weights','/home/doanhdz/Desktop/Computer vision/Lp_Detection/data/ocr/ocr-net.cfg')
with open('/home/doanhdz/Desktop/Computer vision/Lp_Detection/data/ocr/ocr-net.names') as f:
    classes=[line.strip() for line in f.readlines()]
print(classes)	
colors=np.random.uniform(0,255,size=(len(classes),3))
layer_names=net.getLayerNames()
output_layers=[layer_names[i[0]-1] for i in net.getUnconnectedOutLayers()]
img=cv2.imread('/home/doanhdz/Desktop/Computer vision/Lp_Detection/MiAI_LP_Detection_SVM/gray.jpg')
img=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
img = cv2.resize(img,None,fx=0.4, fy=0.4)
height, width, channels = img.shape
blob = cv2.dnn.blobFromImage(img, 0.00392, (240, 80), (0, 0, 0), True, crop=False)
net.setInput(blob)
outs = net.forward(output_layers)
print(outs[0].shape)
# Showing informations on the screen
class_ids = []
confidences = []
boxes = []
for out in outs:
    for detection in out:
          #detection[i] (i=0,1,2,3) la box co-ordinate
          #detection[5] (objecness score)
        scores = detection[5:]
        class_id = np.argmax(scores)  #position of max value element in scores
        confidence = scores[class_id]
        if confidence > 0.5:
            # # Object detected
              # center_x,center_y,w,h la cac so thuc chi vi tri tuong ung cua object trong anh
              # nam trong khoang tu 0 den 1
            center_x = int(detection[0] * width)
            center_y = int(detection[1] * height)
            w = int(detection[2] * width)
            h = int(detection[3] * height)
            # Rectangle coordinates
            x = int(center_x - w / 2)
            y = int(center_y - h / 2)
            
            boxes.append([x, y, w, h])
            confidences.append(float(confidence))
            class_ids.append(class_id)
#This step gets rid of anomalous detections of objects
#non-max suppression
#Repeat Until no boxes to process:
    #Select the box with highest probability of detection.
    #Remove all the boxes with a high IoU with the selected box.
    #Mark the selected box as “processed”
#Soft-NMS
#thay vi loai bo cac boxes co IoU lon ta thay bang confidene=1-IoU
indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
font = cv2.FONT_HERSHEY_PLAIN
plate_infor=""
for i in range(len(boxes)):
    if i in indexes:
        x, y, w, h = boxes[i]
        label = str(classes[class_ids[i]])
        color = colors[i]
        cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
        print("xac suat cua chacracter:",str(round(confidences[i],2)))
        print("label:",label)
        plate_infor+=label
        #cv2.putText(img, label, (x, y + 20), font, 2, color, 2)
        #cv2.putText(img, str(round(confidences[i],2)), (x, y + 38), font, 1, color, 2)
print("Lp_plate:",plate_infor)
window_name = 'yolo'
cv2.resize(img,(470,110))
cv2.imshow(window_name, img)
cv2.waitKey(0)
cv2.destroyAllWindows()