import time
import sys
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
from cv2 import cv2
import numpy as np
from lib_detection import load_model, detect_lp, im2single


# Ham sap xep contour tu trai sang phai
def sort_contours(cnts):

    reverse = False
    i = 0
    #The cv2.boundingRect() function of OpenCV is used to draw an approximate rectangle around the binary image. This function is used mainly  to highlight the region of interest after obtaining contours from an image.
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),key=lambda b: b[1][i], reverse=reverse))
    return cnts

# Dinh nghia cac ky tu tren bien so
char_list =  '0123456789ABCDEFGHKLMNPRSTUVXYZ'

# Ham fine tune bien so, loai bo cac ki tu khong hop ly
def fine_tune(lp):
    newString = ""
    for i in range(len(lp)):
        if lp[i] in char_list:
            newString += lp[i]
    return newString

# Đường dẫn ảnh, các bạn đổi tên file tại đây để thử nhé
#img_path = "/home/doanhdz/Desktop/Computer vision/MiAI_LP_Detection_SVM/test/test01.jpg" #bien vuong
img_path = "/home/doanhdz/Desktop/Computer vision/Lp_Detection/MiAI_LP_Detection_SVM/test/test2.jpg" #bien dai
img=cv2.imread(img_path)
# Load model LP detection
wpod_net_path = "/home/doanhdz/Desktop/Computer vision/Lp_Detection/MiAI_LP_Detection_SVM/wpod-net_update1.json"
wpod_net = load_model(wpod_net_path)

# # Đọc file ảnh đầu vào
# Kích thước lớn nhất và nhỏ nhất của 1 chiều ảnh
Dmax = 608
Dmin = 288
ratio = float(max(img.shape[:2])) / min(img.shape[:2])
# print("ratio",ratio) 1.778501628664495
side = int(ratio * Dmin)
# print("side",side) 512
bound_dim = min(side, Dmax)
start=time.time()
_ , LpImg, lp_type, confidence = detect_lp(wpod_net, im2single(img), bound_dim, lp_threshold=0.5)
print("confidence",confidence)
#print(LpImg[0].shape)  # (200,280,3)
digit_w = 30 # Kich thuoc ki tu
digit_h = 60 # Kich thuoc ki tu
model_svm = cv2.ml.SVM_load('/home/doanhdz/Desktop/Computer vision/Lp_Detection/MiAI_LP_Detection_SVM/svm.xml')
if (len(LpImg)):
    if(lp_type==2):
        
        # Chuyen doi anh bien so
    #Scales, calculates absolute values, and converts the result to 8-bit.
    #alpha la hang so nhan cho phep scale
    #In case of multi-channel arrays, the function processes each channel independently. 
    #When the output is not 8-bit, the operation can be emulated by calling the Mat::convertTo method (or by using matrix expressions) 
    #and then by calculating an absolute value of the result.
        u=LpImg[0].shape[0]
        v=LpImg[0].shape[1]
        LpImg[0] = cv2.convertScaleAbs(LpImg[0], alpha=(255.0))
        LpImgup=LpImg[0][0:100,0:280]
        LpImgdown=LpImg[0][100:200,0:280]
        roi1 = LpImgup
        roi2 = LpImgdown
        #LpImg[0].shape=(110,470,3)
        # Chuyen anh bien so ve gray
        gray1 = cv2.cvtColor( LpImgup, cv2.COLOR_BGR2GRAY)
        gray2 = cv2.cvtColor( LpImgdown, cv2.COLOR_BGR2GRAY)
        #gray.shape=(110,470)

        # Ap dung threshold de phan tach so va nen
        binary1 = cv2.threshold(gray1, 150, 255,cv2.THRESH_BINARY_INV)[1]
        binary2 = cv2.threshold(gray2, 127, 255,cv2.THRESH_BINARY_INV)[1]
        #[1] vi ham threshold tra ve mot tuple chua 2 phan tu la thresh(127) va ma tran duoc phan nguong

        # cv2.imshow("Anh bien so sau threshold", binary)
    

        # Segment kí tự
        #Tao kernel (3,3) dang rectangular
        kernel3 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        #Lam gian no anh
        thre_mor1 = cv2.morphologyEx(binary1, cv2.MORPH_DILATE, kernel3)
        thre_mor2 = cv2.morphologyEx(binary2, cv2.MORPH_DILATE, kernel3)
    
        cont1, _  = cv2.findContours(thre_mor1, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        cont2, _  = cv2.findContours(thre_mor2, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        # img=np.zeros((Ivehicle.shape[0],Ivehicle.shape[1]))
        # img1=np.zeros((Ivehicle.shape[0],Ivehicle.shape[1]))
        boundingBoxes1 = [cv2.boundingRect(c) for c in cont1]
        boundingBoxes2 = [cv2.boundingRect(c) for c in cont2]
        # for c in boundingBoxes:
        #     cv2.rectangle(img, (c[0], c[1]), (c[0] + c[2], c[1] + c[3]), (255, 255, 0), 2)
        # cv2.drawContours(img1,cont,-1,(255,255,0),2)
        # cv2.imshow("cont",img1)
        # cv2.imshow("boundingboxes",img)
        plate_info = ""
        for c in sort_contours(cont1):
            
            (x, y, w, h) = cv2.boundingRect(c)  #(x,y) la vi tri top left (w,h) la chieu da,rong
            ratio = h/w
            if 1.5<=ratio<=3.5: # Chon cac contour dam bao ve ratio w/h
                
                if h/roi1.shape[0]>=0.6: # Chon cac contour cao tu 60% bien so tro len
                    

                    #  draw a green rectangle to visualize the bounding rect
                    cv2.rectangle(roi1, (x, y), (x + w, y + h), (0, 255, 0), 2)

                    # Tach so va predict
                    #Crop
                    curr_num = thre_mor1[y:y+h,x:x+w]
                    curr_num = cv2.resize(curr_num, dsize=(digit_w, digit_h))
                    _, curr_num = cv2.threshold(curr_num, 30, 255, cv2.THRESH_BINARY)
                    curr_num = np.array(curr_num,dtype=np.float32)
                    curr_num = curr_num.reshape(-1, digit_w * digit_h)
                
                    # Dua vao model SVM
                    result = model_svm.predict(curr_num)[1]
                    result = int(result[0, 0])

                    if result<=9: # Neu la so thi hien thi luon
                        
                        result = str(result)
                    else: #Neu la chu thi chuyen bang ASCII
                        result = chr(result)

                    plate_info +=result
        plate_info1=""
        for c in sort_contours(cont2):
            (x, y, w, h) = cv2.boundingRect(c)  #(x,y) la vi tri top left (w,h) la chieu da,rong
            ratio = h/w
            if 1.5<=ratio<=3.5: # Chon cac contour dam bao ve ratio w/h
                if h/roi2.shape[0]>=0.6: # Chon cac contour cao tu 60% bien so tro len

                    #  draw a green rectangle to visualize the bounding rect
                    cv2.rectangle(roi2, (x, y), (x + w, y + h), (0, 255, 0), 2)

                    # Tach so va predict
                    #Crop
                    curr_num = thre_mor2[y:y+h,x:x+w]
                    curr_num = cv2.resize(curr_num, dsize=(digit_w, digit_h))
                    _, curr_num = cv2.threshold(curr_num, 127, 255, cv2.THRESH_BINARY)
                    curr_num = np.array(curr_num,dtype=np.float32)
                    curr_num = curr_num.reshape(-1, digit_w * digit_h)
                
                    # Dua vao model SVM
                    result = model_svm.predict(curr_num)[1]
                    result = int(result[0, 0])

                    if result<=9: # Neu la so thi hien thi luon
                        
                        result = str(result)
                    else: #Neu la chu thi chuyen bang ASCII
                        result = chr(result)

                    plate_info1 +=result
        # Viet bien so len anh
        cv2.putText(img,"text:",(50, 50), cv2.FONT_HERSHEY_PLAIN, 3.0, (0, 0, 255), lineType=cv2.LINE_AA)
        cv2.putText(img,"probs:",(50, 174), cv2.FONT_HERSHEY_PLAIN, 3.0, (0, 0, 255), lineType=cv2.LINE_AA)
        cv2.putText(img,fine_tune(plate_info),(198, 50), cv2.FONT_HERSHEY_PLAIN, 3.0, (0, 0, 255), lineType=cv2.LINE_AA)
        cv2.putText(img,fine_tune(plate_info1),(198, 112), cv2.FONT_HERSHEY_PLAIN, 3.0, (0, 0, 255), lineType=cv2.LINE_AA)
        cv2.putText(img,str(confidence),(198,174),cv2.FONT_HERSHEY_PLAIN, 3.0, (0, 0, 255), lineType=cv2.LINE_AA)
        end=time.time()
        # Hien thi anh
        print("Bien so=", plate_info+plate_info1)
        print(" Execution time: " + str(end-start))
        cv2.imshow("Hinh anh output",img)
        cv2.waitKey()
    elif lp_type==1:
        
        # Chuyen doi anh bien so
        #Scales, calculates absolute values, and converts the result to 8-bit.
        #alpha la hang so nhan cho phep scale
        #In case of multi-channel arrays, the function processes each channel independently. 
        #When the output is not 8-bit, the operation can be emulated by calling the Mat::convertTo method (or by using matrix expressions) 
        #and then by calculating an absolute value of the result.
        LpImg[0] = cv2.convertScaleAbs(LpImg[0], alpha=(255.0))
        roi = LpImg[0]
        #LpImg[0].shape=(110,470,3)
        # Chuyen anh bien so ve gray
        gray = cv2.cvtColor( LpImg[0], cv2.COLOR_BGR2GRAY)
        #gray.shape=(110,470)
        cv2.imshow("gray",gray)
        # Ap dung threshold de phan tach so va nen
        #binary = cv2.threshold(gray, 28, 255,cv2.THRESH_BINARY_INV)[1]
        binary=cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,11,12)
        #[1] vi ham threshold tra ve mot tuple chua 2 phan tu la thresh(127) va ma tran duoc phan nguong

        cv2.imshow("Anh bien so sau threshold", binary)
        cv2.imwrite('Lp_Img.png',binary)

        # Segment kí tự
        #Tao kernel (3,3) dang rectangular
        kernel3 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        #Lam gian no anh
        thre_mor = cv2.morphologyEx(binary, cv2.MORPH_DILATE, kernel3)
        cont, _  = cv2.findContours(thre_mor, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        plate_info = ""
        for c in sort_contours(cont):
            (x, y, w, h) = cv2.boundingRect(c)  #(x,y) la vi tri top left (w,h) la chieu da,rong
            ratio = h/w
            if 1.45<=ratio<=3.5: # Chon cac contour dam bao ve ratio w/h
                if h/roi.shape[0]>=0.6: # Chon cac contour cao tu 60% bien so tro len
                    

                    #  draw a green rectangle to visualize the bounding rect
                    cv2.rectangle(roi, (x, y), (x + w, y + h), (0, 255, 0), 2)
                    
                    # Tach so va predict
                    #Crop
                    curr_num = thre_mor[y:y+h,x:x+w]
                    curr_num = cv2.resize(curr_num, dsize=(digit_w, digit_h))
                    _, curr_num = cv2.threshold(curr_num, 30, 255, cv2.THRESH_BINARY)
                    curr_num = np.array(curr_num,dtype=np.float32)
                    curr_num = curr_num.reshape(-1, digit_w * digit_h)
                
                    # Dua vao model SVM
                    result = model_svm.predict(curr_num)[1]
                    result = int(result[0, 0])

                    if result<=9: # Neu la so thi hien thi luon
                        result = str(result)
                    else: #Neu la chu thi chuyen bang ASCII
                        result = chr(result)

                    plate_info +=result
        cv2.imshow("roi",roi)
        #Viet bien so len anh
        cv2.putText(img,"text:",(50, 50), cv2.FONT_HERSHEY_PLAIN, 3.0, (0, 0, 255), lineType=cv2.LINE_AA)
        cv2.putText(img,"probs:",(50, 112), cv2.FONT_HERSHEY_PLAIN, 3.0, (0, 0, 255), lineType=cv2.LINE_AA)
        cv2.putText(img,fine_tune(plate_info),(198, 50), cv2.FONT_HERSHEY_PLAIN, 3.0, (0, 0, 255), lineType=cv2.LINE_AA)
        cv2.putText(img,str(confidence,),(198,112),cv2.FONT_HERSHEY_PLAIN, 3.0, (0, 0, 255), lineType=cv2.LINE_AA)
        end=time.time()
        print("executive time" ,str(end-start))
        # Hien thi anh
        print("Bien so=", plate_info)
        cv2.imshow("Hinh anh output",img)
        cv2.waitKey()
cv2.destroyAllWindows()
