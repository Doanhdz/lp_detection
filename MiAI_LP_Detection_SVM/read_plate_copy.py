import time
import sys
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
from cv2 import cv2
import numpy as np
from lib_detection import load_model, detect_lp, im2single


# Ham sap xep contour tu trai sang phai
def sort_contours(cnts):

    reverse = False
    i = 0
    #The cv2.boundingRect() function of OpenCV is used to draw an approximate rectangle around the binary image. This function is used mainly  to highlight the region of interest after obtaining contours from an image.
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),key=lambda b: b[1][i], reverse=reverse))
    return cnts

# Dinh nghia cac ky tu tren bien so
char_list =  '0123456789ABCDEFGHKLMNPRSTUVXYZ'

# Ham fine tune bien so, loai bo cac ki tu khong hop ly
def fine_tune(lp):
    newString = ""
    for i in range(len(lp)):
        if lp[i] in char_list:
            newString += lp[i]
    return newString

# Đường dẫn ảnh, các bạn đổi tên file tại đây để thử nhé
#img_path = "/home/doanhdz/Desktop/Computer vision/MiAI_LP_Detection_SVM/test/test01.jpg" #bien vuong
img_path = "/home/doanhdz/Desktop/Computer vision/Lp_Detection/MiAI_LP_Detection_SVM/test/test2.jpg" #bien dai
img=cv2.imread(img_path)
# Load model LP detection
wpod_net_path = "/home/doanhdz/Desktop/Computer vision/Lp_Detection/MiAI_LP_Detection_SVM/wpod-net_update1.json"
wpod_net = load_model(wpod_net_path)

# # Đọc file ảnh đầu vào
# Kích thước lớn nhất và nhỏ nhất của 1 chiều ảnh
Dmax = 608
Dmin = 288
ratio = float(max(img.shape[:2])) / min(img.shape[:2])
# print("ratio",ratio) 1.778501628664495
side = int(ratio * Dmin)
# print("side",side) 512
bound_dim = min(side, Dmax)
start=time.time()
_ , LpImg, lp_type, confidence = detect_lp(wpod_net, im2single(img), bound_dim, lp_threshold=0.5)
print("confidence",confidence)
#print(LpImg[0].shape)  # (200,280,3)
digit_w = 30 # Kich thuoc ki tu
digit_h = 60 # Kich thuoc ki tu
#model_svm = cv2.ml.SVM_load('/home/doanhdz/Desktop/Computer vision/MiAI_LP_Detection_SVM/svm.xml')
if (len(LpImg)):
    # if(lp_type==2):
        
    #     # Chuyen doi anh bien so
    # #Scales, calculates absolute values, and converts the result to 8-bit.
    # #alpha la hang so nhan cho phep scale
    # #In case of multi-channel arrays, the function processes each channel independently. 
    # #When the output is not 8-bit, the operation can be emulated by calling the Mat::convertTo method (or by using matrix expressions) 
    # #and then by calculating an absolute value of the result.
    #     u=LpImg[0].shape[0]
    #     v=LpImg[0].shape[1]
    #     LpImg[0] = cv2.convertScaleAbs(LpImg[0], alpha=(255.0))
    #     LpImgup=LpImg[0][0:100,0:280]
    #     LpImgdown=LpImg[0][100:200,0:280]
    #     roi1 = LpImgup
    #     roi2 = LpImgdown
    #     #LpImg[0].shape=(110,470,3)
    #     # Chuyen anh bien so ve gray
    #     gray1 = cv2.cvtColor( LpImgup, cv2.COLOR_BGR2GRAY)
    #     gray2 = cv2.cvtColor( LpImgdown, cv2.COLOR_BGR2GRAY)
    #     #gray.shape=(110,470)

    #     # Ap dung threshold de phan tach so va nen
    #     binary1 = cv2.threshold(gray1, 150, 255,cv2.THRESH_BINARY_INV)[1]
    #     binary2 = cv2.threshold(gray2, 127, 255,cv2.THRESH_BINARY_INV)[1]
    #     #[1] vi ham threshold tra ve mot tuple chua 2 phan tu la thresh(127) va ma tran duoc phan nguong

    #     # cv2.imshow("Anh bien so sau threshold", binary)
    

    #     # Segment kí tự
    #     #Tao kernel (3,3) dang rectangular
    #     kernel3 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    #     #Lam gian no anh
    #     thre_mor1 = cv2.morphologyEx(binary1, cv2.MORPH_DILATE, kernel3)
    #     thre_mor2 = cv2.morphologyEx(binary2, cv2.MORPH_DILATE, kernel3)
    
    #     cont1, _  = cv2.findContours(thre_mor1, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    #     cont2, _  = cv2.findContours(thre_mor2, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    #     # img=np.zeros((Ivehicle.shape[0],Ivehicle.shape[1]))
    #     # img1=np.zeros((Ivehicle.shape[0],Ivehicle.shape[1]))
    #     boundingBoxes1 = [cv2.boundingRect(c) for c in cont1]
    #     boundingBoxes2 = [cv2.boundingRect(c) for c in cont2]
    #     # for c in boundingBoxes:
    #     #     cv2.rectangle(img, (c[0], c[1]), (c[0] + c[2], c[1] + c[3]), (255, 255, 0), 2)
    #     # cv2.drawContours(img1,cont,-1,(255,255,0),2)
    #     # cv2.imshow("cont",img1)
    #     # cv2.imshow("boundingboxes",img)
    #     plate_info = ""
    #     for c in sort_contours(cont1):
            
    #         (x, y, w, h) = cv2.boundingRect(c)  #(x,y) la vi tri top left (w,h) la chieu da,rong
    #         ratio = h/w
    #         if 1.5<=ratio<=3.5: # Chon cac contour dam bao ve ratio w/h
                
    #             if h/roi1.shape[0]>=0.6: # Chon cac contour cao tu 60% bien so tro len
                    

    #                 #  draw a green rectangle to visualize the bounding rect
    #                 cv2.rectangle(roi1, (x, y), (x + w, y + h), (0, 255, 0), 2)

    #                 # Tach so va predict
    #                 #Crop
    #                 curr_num = thre_mor1[y:y+h,x:x+w]
    #                 curr_num = cv2.resize(curr_num, dsize=(digit_w, digit_h))
    #                 _, curr_num = cv2.threshold(curr_num, 30, 255, cv2.THRESH_BINARY)
    #                 curr_num = np.array(curr_num,dtype=np.float32)
    #                 curr_num = curr_num.reshape(-1, digit_w * digit_h)
                
    #                 # Dua vao model SVM
    #                 result = model_svm.predict(curr_num)[1]
    #                 result = int(result[0, 0])

    #                 if result<=9: # Neu la so thi hien thi luon
                        
    #                     result = str(result)
    #                 else: #Neu la chu thi chuyen bang ASCII
    #                     result = chr(result)

    #                 plate_info +=result
    #     plate_info1=""
    #     for c in sort_contours(cont2):
    #         (x, y, w, h) = cv2.boundingRect(c)  #(x,y) la vi tri top left (w,h) la chieu da,rong
    #         ratio = h/w
    #         if 1.5<=ratio<=3.5: # Chon cac contour dam bao ve ratio w/h
    #             if h/roi2.shape[0]>=0.6: # Chon cac contour cao tu 60% bien so tro len

    #                 #  draw a green rectangle to visualize the bounding rect
    #                 cv2.rectangle(roi2, (x, y), (x + w, y + h), (0, 255, 0), 2)

    #                 # Tach so va predict
    #                 #Crop
    #                 curr_num = thre_mor2[y:y+h,x:x+w]
    #                 curr_num = cv2.resize(curr_num, dsize=(digit_w, digit_h))
    #                 _, curr_num = cv2.threshold(curr_num, 127, 255, cv2.THRESH_BINARY)
    #                 curr_num = np.array(curr_num,dtype=np.float32)
    #                 curr_num = curr_num.reshape(-1, digit_w * digit_h)
                
    #                 # Dua vao model SVM
    #                 result = model_svm.predict(curr_num)[1]
    #                 result = int(result[0, 0])

    #                 if result<=9: # Neu la so thi hien thi luon
                        
    #                     result = str(result)
    #                 else: #Neu la chu thi chuyen bang ASCII
    #                     result = chr(result)

    #                 plate_info1 +=result
    #     # Viet bien so len anh
    #     cv2.putText(img,"text:",(50, 50), cv2.FONT_HERSHEY_PLAIN, 3.0, (0, 0, 255), lineType=cv2.LINE_AA)
    #     cv2.putText(img,"probs:",(50, 174), cv2.FONT_HERSHEY_PLAIN, 3.0, (0, 0, 255), lineType=cv2.LINE_AA)
    #     cv2.putText(img,fine_tune(plate_info),(198, 50), cv2.FONT_HERSHEY_PLAIN, 3.0, (0, 0, 255), lineType=cv2.LINE_AA)
    #     cv2.putText(img,fine_tune(plate_info1),(198, 112), cv2.FONT_HERSHEY_PLAIN, 3.0, (0, 0, 255), lineType=cv2.LINE_AA)
    #     cv2.putText(img,str(confidence),(198,174),cv2.FONT_HERSHEY_PLAIN, 3.0, (0, 0, 255), lineType=cv2.LINE_AA)
    #     end=time.time()
    #     # Hien thi anh
    #     print("Bien so=", plate_info+plate_info1)
    #     print(" Execution time: " + str(end-start))
    #     cv2.imshow("Hinh anh output",img)
    #     cv2.waitKey()
    if lp_type==1:
        LpImg[0] = cv2.convertScaleAbs(LpImg[0], alpha=(255.0))
        roi = LpImg[0]
        gray = cv2.cvtColor( LpImg[0], cv2.COLOR_BGR2GRAY)
        #gray.shape=(110,470)
        img=gray
        net=cv2.dnn.readNet('/home/doanhdz/Desktop/Computer vision/Lp_Detection/data/ocr/ocr-net.weights','/home/doanhdz/Desktop/Computer vision/Lp_Detection/data/ocr/ocr-net.cfg')
        with open('/home/doanhdz/Desktop/Computer vision/Lp_Detection/data/ocr/ocr-net.names') as f:
            classes=[line.strip() for line in f.readlines()]
        print(classes)	
        colors=np.random.uniform(0,255,size=(len(classes),3))
        layer_names=net.getLayerNames()
        output_layers=[layer_names[i[0]-1] for i in net.getUnconnectedOutLayers()]
        img=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
        img = cv2.resize(img,None,fx=0.4, fy=0.4)
        height, width, channels = img.shape
        blob = cv2.dnn.blobFromImage(img, 0.00392, (240, 80), (0, 0, 0), True, crop=False)
        net.setInput(blob)
        outs = net.forward(output_layers)
        print(outs[0].shape)
# Showing informations on the screen
        class_ids = []
        confidences = []
        boxes = []
        for out in outs:
            for detection in out:
          #detection[i] (i=0,1,2,3) la box co-ordinate
          #detection[5] (objecness score)
                scores = detection[5:]
                class_id = np.argmax(scores)  #position of max value element in scores
                confidence = scores[class_id]
                if confidence > 0.5:
            # # Object detected
              # center_x,center_y,w,h la cac so thuc chi vi tri tuong ung cua object trong anh
              # nam trong khoang tu 0 den 1
                    center_x = int(detection[0] * width)
                    center_y = int(detection[1] * height)
                    w = int(detection[2] * width)
                    h = int(detection[3] * height)
            # Rectangle coordinates
                    x = int(center_x - w / 2)
                    y = int(center_y - h / 2)
            
                    boxes.append([x, y, w, h])
                    confidences.append(float(confidence))
                    class_ids.append(class_id)

        indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
        font = cv2.FONT_HERSHEY_PLAIN
        plate_infor=""
        for i in range(len(boxes)):
            if i in indexes:
                x, y, w, h = boxes[i]
                label = str(classes[class_ids[i]])
                color = colors[i]
                cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
                print("xac suat cua chacracter:",str(round(confidences[i],2)))
                print("label:",label)
                plate_infor +=label
        
        end=time.time()
        print("executive time" ,str(end-start))
        # Hien thi anh
        print("Bien so=", str(plate_infor))
        cv2.imshow("Hinh anh output",img)
        cv2.waitKey(0)
cv2.destroyAllWindows()
