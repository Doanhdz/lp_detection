Labels for images inside the following datasets:

AOLP Law Enforcement  : http://aolpr.ntust.edu.tw/lab/
SSIG SegPlate Database: http://www.ssig.dcc.ufmg.br/ssig-segplate-database/
Cars dataset          : https://ai.stanford.edu/~jkrause/cars/car_dataset.html

Data format: CSV

N,tlx,trx,brx,blx,tly,try,bry,bly,LABEL,

where:
	N = number or corners (fixed in 4)
	tl[x,y] = top left corner
	tr[x,y] = top right corner
	br[x,y] = bottom right corner
	bl[x,y] = bottom left corner

*all values are normalized between 0 and 1.
*to get pixel coordinates, multiply x and y by the image width and height, respectively.

